FROM node:alpine
CMD ["apk", "add", "curl"]
WORKDIR /app
COPY package.json .
RUN npm install
COPY . ./
EXPOSE 3000
CMD ["node","index.js"]
